## CMYK Readme
  
### Installing and running the game
  1. Download the game [here.](https://gitlab.com/sajay/CYMK.git)
  2. Unzip the file to some directory. 
  3. Navigate to unzip_directory/CMYK/Export and double click CYMK.exe
  
### Playing the Game
After starting the game from the main menu, use touch input to drag the character's desired clothes on to her.
