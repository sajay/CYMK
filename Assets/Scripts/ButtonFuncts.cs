﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFuncts : MonoBehaviour{
	public static ButtonFuncts instance;

	void Start(){
		instance = this;
	}

	// General Global Functions
	// Due to limitations in unity they can't be static
	public void Continue_Game(){
		Initiate.Fade(2);
	}
	public void Start_New_Game(){
		//ClearData
		SaveState.clearGame();
		Continue_Game();
	}
	public void To_Gallery(){
		Initiate.Fade(1);
	}
	public void Exit_Game(){
		Application.Quit();
	}
	public void To_Main(){
		Initiate.Fade(0);
	}
}