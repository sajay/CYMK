﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Level_Controller : MonoBehaviour {
	// //This holds the integer of the item of clothing that the girl wants
	public int[] solution;

	public String promptText;

	//This holds the girl that we will be dressing prefab
	public GameObject BODY;

	//These hold all the PREFABS of articles of clothing that the level will supply
	//Unity refuses to allow assignment to complex structure, so duplicate code is unavoidable
	//	thanks to this annoying named data structure
	public GameObject[] SHIRTS;
	public GameObject[] PANTS;
	public GameObject[] SHOES;
	public GameObject[] EARRINGS;

	//When the game controller decides to initiate a level,
	//this function is called on the appropriate level's gameobject
	public void StartLevel(){
		//Assign the text showing what girl is asking for
		Globals.instance.promptText.text = promptText;

		//Set a default open tab
		Globals.instance.OpenRack(0);

		//Assign the body we'll be dressing's instance
		Globals.instance.body = Instantiate(BODY, new Vector3(.245f,.3f,0), Quaternion.Euler(new Vector3(0,0,0)));
		//Globals.instance.body.transform.localScale = new Vector3 (.2f, .2f, 1f);

		//Spawn and assign the shirt to lists
		for(int i = 0; i < SHIRTS.Length;i++){
			Globals.instance.cloths[0].Add(Instantiate(SHIRTS[i], new Vector3(0,0,0), Quaternion.Euler(new Vector3(0,0,0))));
		}
		for(int i = 0; i < PANTS.Length;i++){
			Globals.instance.cloths[1].Add(Instantiate(PANTS[i], new Vector3(0,0,0), Quaternion.Euler(new Vector3(0,0,0))));
		}
		for(int i = 0; i < SHOES.Length;i++){
			Globals.instance.cloths[2].Add(Instantiate(SHOES[i], new Vector3(0,0,0), Quaternion.Euler(new Vector3(0,0,0))));
		}
		for(int i = 0; i < EARRINGS.Length;i++){
			Globals.instance.cloths[3].Add(Instantiate(EARRINGS[i], new Vector3(0,0,0), Quaternion.Euler(new Vector3(0,0,0))));
		}

		//Assign solution to clothing script
		for (int i = 0; i < solution.Length; i++){
			Globals.instance.cloths[i][solution[i]].GetComponent<ClothScript>().correct = true;
		}
	}
}