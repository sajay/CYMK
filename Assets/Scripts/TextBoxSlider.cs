﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextBoxSlider : MonoBehaviour {
	bool held = false;

	float mouseDelta;

	public Transform textObject;
	
	// Update is called once per frame
	void Update () {
		if (held){
			textObject.transform.position = new Vector3(
				textObject.position.x,
				Mathf.Clamp(mouseDelta + Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0, 50),
				textObject.position.z
			);
		}
	}

	void OnMouseDown(){
		held = true;

		mouseDelta = textObject.position.y - Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
	}
	void OnMouseUp(){
		held = false;
	}
}
