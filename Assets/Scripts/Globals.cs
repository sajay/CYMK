﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class holds game scene globals
public class Globals : MonoBehaviour {
	//Number of incorrect attempts
	public int failures = 0;

	public static Globals instance;

	public const int LEVELS = 6;

	public GameObject COMPLETED_GAME_DISPLAY;

	public Text promptText;

	public GameObject PROMPT_CONTAINER;
	public GameObject SCORE_CONTAINER;
	public GameObject INKS_CONTAINER;

	//This holds the girl that we will be dressing instance
	public GameObject body;

	public Level_Controller[] LEVEL_CONTROLLERS = new Level_Controller[LEVELS];

	public GameObject[] racks = new GameObject[4];

	public static List<GameObject> SCORE_CARTRAGES;

	public int level = 0;

	//These hold all the instances of articles of clothing that the level currently has
	//shirts  	//pants 	//shoes 	//earrings
	public List<GameObject>[] cloths = new List<GameObject>[4]{
		new List<GameObject>(),
		new List<GameObject>(),
		new List<GameObject>(),
		new List<GameObject>()
	};


	// Use this for initialization
	void Start() {
		instance = this;

		LEVEL_CONTROLLERS = this.GetComponents<Level_Controller>();

		SaveState.readState();

		for(int i = 0; i < LEVELS; i++){
			if (SaveState.getState(i).completed){
				level = i + 1;
			}
			else{
				break;
			}
		}

		if (level >= LEVELS){
			COMPLETED_GAME_DISPLAY.SetActive(true);
			return;
		}

		SCORE_CARTRAGES = new List<GameObject>();
		foreach(Transform cartrage in INKS_CONTAINER.transform){
			SCORE_CARTRAGES.Add(cartrage.gameObject);
		}

		LEVEL_CONTROLLERS[level].StartLevel();

		ShowPrompt();
		ResetRack();
	}

	public GameObject Pause_Menu;
    public GameObject Settings_Menu;

	//These buttons are found on the pause menu
	public void Pause () {
		Pause_Menu.SetActive(true);
        Settings_Menu.SetActive(false);
	}
	public void UnPause() {
		Pause_Menu.SetActive(false);
	}
	public void Setting(){
        Settings_Menu.SetActive(true);
        UnPause();
		Debug.Log("Settings set!");
	}


	#region swappingtabs

	//These functions change the background color and open the cloths on a newly opened rack
	public void OpenRack(int rack){
		for(int i = 0; i < racks.Length; i++){
			racks[i].SetActive(false);
		}
		racks[rack].SetActive(true);
	}

	//Swap pages within a tab
	static bool currPage = false;
	public void SwapPage(bool newPage){
		if (newPage != currPage){
			currPage = newPage;
			foreach(GameObject tab in racks){
				StartCoroutine(MovePagesLeft(
					tab.transform.GetChild(0).gameObject,
					new Vector3(newPage? -16: -2, 9f ,0f)
				));

				StartCoroutine(MovePagesLeft(
					tab.transform.GetChild(1).gameObject,
					new Vector3(newPage? -2: -16, 9f ,0f)
				));
			}
		}
	}

	//This function moves all the pages leftward
	static IEnumerator MovePagesLeft(GameObject Page, Vector3 newPosition){
		//if routine lasts too long, force movement and drop routine
		int safeGuard = 0;
		
		float speed = 10f;

		while (Vector3.Distance(Page.transform.position, newPosition) > .001f){
			safeGuard++;
			if (safeGuard > 30){
				//If safeGuard exceeded just drop routine in what ever position
				//This is ok because another douplicate routine is causing the hangup
				//We can rely on the other routine to deal with the mess once we leave
				yield break;
			}
			Page.transform.localPosition = Vector3.Lerp(Page.transform.position, newPosition, speed * Time.deltaTime);

			//If the page is so far left, throw it around to the other side...
			//	adjust the target, and continue as if nothing happened
			if (Page.transform.position.x < -14){
				Page.transform.localPosition = new Vector3(
					Page.transform.position.x + 28,
					Page.transform.position.y,
					Page.transform.position.z
				);
				newPosition = new Vector3(newPosition.x + 28, newPosition.y, newPosition.z);
			}
			yield return new WaitForSeconds (.015f);
		}

	}
	#endregion

	#region swapStatus
	//Swaps between the prompt and the score
	public void ShowPrompt(){
		StartCoroutine(SwapStatusAnimate(PROMPT_CONTAINER, new Vector3(1.5f,-10f,1f)));
		StartCoroutine(SwapStatusAnimate(SCORE_CONTAINER, new Vector3(-11f,-10f,1f)));
	}
	public void ShowScore(){
		StartCoroutine(SwapStatusAnimate(PROMPT_CONTAINER, new Vector3(11f,-10f,1f)));
		StartCoroutine(SwapStatusAnimate(SCORE_CONTAINER, new Vector3(-1.5f,-10f,1f)));
	}

	static IEnumerator SwapStatusAnimate(GameObject box, Vector3 newPosition){
		//if routine lasts too long, force movement and drop routine
		int safeGuard = 0;
		
		float speed = 10f;

		while (Vector3.Distance(box.transform.position, newPosition) > .01f){
			safeGuard++;
			if (safeGuard > 20){
				//If safeGuard exceeded just drop routine in what ever position
				//This is ok because another douplicate routine is causing the hangup
				//We can rely on the other routine to deal with the mess once we leave
				yield break;
			}
			
			box.transform.position = Vector3.Lerp(box.transform.position, newPosition, speed * Time.deltaTime);

			yield return new WaitForSeconds (.015f);
		}

	}
	#endregion

	#region scoring
	//Deleting the objects in the Score GameObject is all it takes to
	//	Change the maximum score
	public static void SetScore(int newScore){
		for (int i = 0; i < SCORE_CARTRAGES.Count; i++){
			if (i < newScore){
				SCORE_CARTRAGES[i].transform.GetChild(0).gameObject.SetActive(true);
				SCORE_CARTRAGES[i].transform.GetChild(1).gameObject.SetActive(false);
			}
			else{
				SCORE_CARTRAGES[i].transform.GetChild(0).gameObject.SetActive(false);
				SCORE_CARTRAGES[i].transform.GetChild(1).gameObject.SetActive(true);
			}
		}
	}
	#endregion

	//Functions completing or failing level
	//Success
	IEnumerator Continue(){
		//Update save data
		SaveState.setState(level, true, failures);
		SaveState.writeState();

		WaitForSeconds delay = new WaitForSeconds(.01f);

		//End location for score
		Vector3 endSpot = new Vector3(0, 3, 0);

		//Move Score to center
		while(
			Mathf.Abs(SCORE_CONTAINER.transform.position.x - endSpot.x) > .01 ||
			Mathf.Abs(SCORE_CONTAINER.transform.position.y - endSpot.y) > .01
		){
			SCORE_CONTAINER.transform.position = Vector3.Lerp(SCORE_CONTAINER.transform.position, endSpot, .1f);

			yield return delay;
		}

		yield return new WaitForSeconds(1f);

		//Reload level
		ButtonFuncts.instance.Continue_Game();
	}

	//Failure
	IEnumerator Retry(){
		WaitForSeconds delay = new WaitForSeconds(.01f);

		//End location for score
		Vector3 endSpot = new Vector3(0, 3, 0);

		//Move Score to center
		while(
			Mathf.Abs(SCORE_CONTAINER.transform.position.x - endSpot.x) > .01 ||
			Mathf.Abs(SCORE_CONTAINER.transform.position.y - endSpot.y) > .01
		){
			SCORE_CONTAINER.transform.position = Vector3.Lerp(SCORE_CONTAINER.transform.position, endSpot, .1f);

			yield return delay;
		}

		yield return new WaitForSeconds(1f);

		//Reload level
		ButtonFuncts.instance.Continue_Game();
	}


	//This function iterates the current racks of cloths, and places the evenly apart on the rack
	public void ResetRack(){
		//iterate racks
		for(int i = 0; i < cloths.Length;i++){
			int firstPageLength = 0 ;
			if (cloths[i].Count <= 3){
				//Give all to first page
				firstPageLength = cloths[i].Count;
				//Disable second tab if it's not needed
				racks[i].transform.GetChild(1).gameObject.SetActive(false);
				//Set to first page because second is empty
				SwapPage(false);
			}
			else{
				//Distribute evenly
				firstPageLength = (int)Mathf.Ceil((float)cloths[i].Count / 2);
			}

			//iterate cloths on rack
			for (int k = 0; k < cloths[i].Count;k++){
				//Assign page //0 first page //1 second page
				bool assignPage =  (k + 1) > firstPageLength;

				//Assign number in this page
				int assignN = assignPage? cloths[i].Count - firstPageLength: firstPageLength;

				//Assign this index in the page
				int assignI = assignPage? (k + 1) - firstPageLength: (k + 1);

				//Assign space between cloths and walls
				float assignDeltaX = 20 / (assignN + 1);

				//Assign x
				float assignX = (assignDeltaX * assignI) - 9;

				//Assign cloths Rack and page
				cloths[i][k].transform.SetParent(racks[i].transform.GetChild(
						assignPage? 1 : 0
				));	

				float Y;
				switch (i) {
				case 0:
					Y = -1.96f;
					break;
				case 1:
					assignX += 2;
					Y = 2.87f;
					break;
				case 2:
				case 3:
				default:
					Y = 0;
					break;
				}

				//Assign X position
				cloths[i][k].transform.localPosition = new Vector3(assignX,Y,-1f);
			}
		}
	}

	public void SetNextRack(){
		for(int i = 0; i < cloths.Length; i++){
			if (cloths[i].Count != 0){
				OpenRack(i);
				return;
			}
		}

		//Runs on completion of level
		StartCoroutine("Continue");

		//Sets end of level score displays
		SaveState.setState(level, true, failures);
		SaveState.writeState();
	}

	public void ClearRack(int rack_index){
		for (int i = cloths[rack_index].Count-1; i >= 0; i--){
			Destroy(cloths[rack_index][i]);
			cloths[rack_index].RemoveAt(i);
		}
		SetNextRack();
	}

	public void RemoveFromRack(GameObject cloth, bool correct){
		for (int i = 0; i < cloths.Length; i++){
			if (cloths[i].Contains(cloth)){
				cloths[i].Remove(cloth);
				if (correct){
					switch (i){
						case 0:
							cloth.transform.position = new Vector3(-.14f,-.51f,0);
							break;
						case 1:
							cloth.transform.position = new Vector3(1.64f,-.43f,0);
							break;
						case 2:
							cloth.transform.position = new Vector3 (-.28f, -7.17f, 0);
							break;
						case 3:
							cloth.transform.position = new Vector3(.24f, 4.37f, 0);
							break;
					}
					ClearRack(i);
				}
				break;
			}
		}
	}

	public void RemoveCloth(GameObject cloth){
		ShowScore();

		RemoveFromRack(cloth, false);
		Destroy(cloth);
		ResetRack();
		
		failures++;
		Globals.SetScore(Globals.SCORE_CARTRAGES.Count - failures);
		if (failures >= Globals.SCORE_CARTRAGES.Count){
			//Runs on failure of level
			StartCoroutine("Retry");
		}
	}
	public void AddCloth(GameObject cloth){
		ShowScore();

		RemoveFromRack(cloth, true);
		ResetRack();
		cloth.transform.SetParent(body.transform);
		Destroy(cloth.GetComponent<BoxCollider2D>());
	}
}