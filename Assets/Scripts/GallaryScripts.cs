﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GallaryScripts : MonoBehaviour {
	public Transform Portraits;

	void Start(){
		SaveState.readState();
		for (int i = 0; i < Globals.LEVELS; i++) {
			if (SaveState.getState(i).completed){
				Portraits.GetChild(i).GetChild(0).gameObject.SetActive(true);
			}
			else{
				Portraits.GetChild(i).GetChild(2).gameObject.SetActive(true);
			}
		}
	}
}