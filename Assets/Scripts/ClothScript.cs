﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothScript : MonoBehaviour {
	public bool correct = false;
	bool held = false;

	Vector2 mouseDelta;
	
	// Update is called once per frame
	void Update () {
		if (held){
			Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));

			this.transform.position = new Vector3(
				point.x + mouseDelta.x,
				point.y + mouseDelta.y,
				0
			);
		}
	}

	void OnMouseDown(){
		held = true;

		Vector2  mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);


		mouseDelta = new Vector2(
			this.transform.position.x - mouse.x,
			this.transform.position.y - mouse.y
		);
	}
	void OnMouseUp(){
		held = false;

		if (Globals.instance.body.GetComponent<CapsuleCollider2D>().bounds.Contains(this.transform.position)){
			if (this.correct){
				//Snap
				Globals.instance.AddCloth(this.gameObject);
			}
			else{
				Globals.instance.RemoveCloth(this.gameObject);
			}
		}
		else{
			Globals.instance.ResetRack();
		}
	}
}
