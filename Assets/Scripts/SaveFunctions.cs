using UnityEngine;
using System;
using System.IO;

//Record used per level
[Serializable]
public struct lvlState{
	public bool completed;
	public int failures;
}

public static class SaveState{
	//Holds all data
	private static lvlState[] saveData = new lvlState[Globals.LEVELS];

	//Writes to variable
	public static void setState(int lvl, bool completed, int failures){
		saveData[lvl].completed = completed;
		saveData[lvl].failures = failures;
	}

	//Loads from variable
	public static lvlState getState(int lvl){
		return saveData[lvl];
	}

	//Overwrites file with newState
	public static void writeState(){
		System.IO.File.WriteAllText(Application.persistentDataPath + "/save.json", JsonHelper.ToJson(saveData, true));
	}

	//Only returns save state from file
	public static void readState(){
		if (File.Exists(Application.persistentDataPath + "/save.json")){
			String data = System.IO.File.ReadAllText(Application.persistentDataPath + "/save.json");
			saveData = JsonHelper.FromJson<lvlState>(data);
		}
		else{
			for(int i = 0; i < saveData.Length; i++){
	    		setState(i, false, 0);
			}
		}
    }

    //Clears variable
    public static void clearGame(){
		for(int i = 0; i < saveData.Length; i++){
    		setState(i, false, 0);
		}
		writeState();
    }
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}