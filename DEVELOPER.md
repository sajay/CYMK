## CYMK Developer Documentation
  

#### Summary #
CYMK is an educational 2d app game created in the unity engine, meant to educate about color schemes using clothing and an inkjet printer theme.

#### Data Storage
Currently our project plans to save data state by writing a floating point number into a file as a string.
  
#### UI Design and Functionality
Buttons and interactibles are intended to be large in order to facilitate easy use of touch screen controls.
The main menu will allow navigation to continue a game in progress, start a new game, visit the gallery to see snapshots of completed levels, or to exit the game.
The play space will allow the user to drag clothes onto the model in order to create outfit combinations. Additionally, the type of clothing available to drag is grouped by type in tabs across the top of the screen.